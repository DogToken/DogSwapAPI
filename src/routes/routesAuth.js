'use strict';

import ControllerAuth from '../controllers/ControllerAuth.js';

/**
 * Mounts all routes of ControllerAuth.
 * @param  {Object} app    Express Server Object
 * @param  {Object} config Configs from config.json
 */
export default function routesAuth(app, config) {
    const controllerAuth = new ControllerAuth(config);
    app.post('/login', controllerAuth.login);
}