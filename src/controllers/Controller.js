'use strict';

import ErrorBadRequest from '../errors/ErrorBadRequest.js';
import ErrorNotFound from '../errors/ErrorNotFound.js';

/**
 * Basic Controller class.
 * 
 * @info Methods intended for the use in the router have to
 * be written in arrow notation in order to use this.
 * @see https://stackoverflow.com/questions/33798933/nodejs-express-routes-as-es6-classes
 */
export default class Controller {

  /**
   * handesl commen errors like not found or badrequest.
   * @param  {Object} e   Error object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  handleResponsError(e, res) {
    if (e instanceof ErrorBadRequest) {
      return res.status(400).json(
        {message: e.message}
      );
    } else if(e instanceof ErrorNotFound) {

      return res.status(404).json(
        {message: e.message}
      );
    } else {
      return res.status(500).json(
        {message: e.message}
      );
    }
  }
}
