'use strict';

import Controller from './Controller.js';

import RepositoryToken from './../repositorys/RepositoryToken.js';

import ErrorBadRequest from '../errors/ErrorBadRequest.js';
import ErrorNotFound from '../errors/ErrorNotFound.js';


/**
 * Token controller to use in Routes.
 */
export default class ControllerToken extends Controller {

  /**
   * Can't be private async methods need it.
   */
  #repository;

  /**
   * Constructs the token controller.
   * @param  {Object} config Basic configurations
   */
  constructor(config) {
    super();
    this.#repository = new RepositoryToken(config.database);
  }

  /**
   * Returns all token as a response.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  getAllTokens = async (req, res) => {
    const tokens = await this.#repository.findAll();
  
    return res.status(200).json(
      tokens
    );
  };

  /**
   * Returns a specific token as a response.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  getToken = async (req, res) => {
    try {
      const id = this.#validateId(
        req.params.id
      );

      const token = await this.#repository.findById(id);
      
      return res.status(200).json(
        token
      );
    } catch(e) {
      return this.handleResponsError(e, res);
    }
  };

  /**
   * Adds a token to the database and responses with
   * the new token from the database.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  postToken = async (req, res) => {
    try {
      const token = this.#validatePostToken(
        req.body
      );

      return res.status(200).json(
        await this.#repository.save(token)
      );
    } catch(e) {
      return this.handleResponsError(e, res);
    }
  };

  /**
   * Update Token with all infos.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  putToken = async (req, res) => {
    try {
      const token = this.#validatePutToken(
        req.body,
        req.params.id
      );

      return res.status(200).json(
        await this.#repository.save(token)
      );
    } catch(e) {
      return this.handleResponsError(e, res);
    }
  };

  /**
   * Update Token with patial info.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  patchToken = async (req, res) => {
    try {
      const token = this.#validatePatchToken(
        req.body,
        req.params.id
      );

      return res.status(200).json(
        await this.#repository.save(token)
      );
    } catch(e) {
      return this.handleResponsError(e, res);
    }
  };

  /**
   * Removes token.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  deleteToken = async (req, res) => {
    try {
      const id = this.#validateId(
        req.params.id
      );

      await this.#repository.deleteById(id);
      return res.status(204).send();
    } catch(e) {
      return this.handleResponsError(e, res);
    }
  };

  /**
   * Validates token from post request.
   * @param  {Object} token Token object.
   * @return {Object}       Clone of token object.
   */
  #validatePostToken(token) {
    const _token = this.#validateToken(token);

    if(_token.id)
      throw new ErrorBadRequest(
        "On POST id of token can't be set."
      );

    return _token;
  }

  /**
   * Validates token from put request.
   * @param  {Object} token Token object.
   * @return {Object}       Clone of token object.
   */
  #validatePutToken(token, id) {
    const _token = this.#validateToken(token);

    if(!_token.name)
      throw new ErrorBadRequest(
        "On put name of token has to be set."
      );

    if(!_token.symbol)
      throw new ErrorBadRequest(
        "On put name of symbol has to be set."
      );

    if(!_token.type)
      throw new ErrorBadRequest(
        "On put type of token has to be set."
      );

    if(!_token.address)
      throw new ErrorBadRequest(
        "On put address of token has to be set."
      );

    if(!_token.decimals)
      throw new ErrorBadRequest(
        "On put decimals of token has to be set."
      );

    if(!_token.image)
      throw new ErrorBadRequest(
        "On put image of token has to be set."
      );

    _token.id = this.#validateId(id);

    return _token;
  }

  /**
   * Validates token from patch request.
   * @param  {Object} token Token object.
   * @return {Object}       Clone of token object.
   */
  #validatePatchToken(token, id) {
    const _token = this.#validateToken(token);

    _token.id = this.#validateId(id);

    return _token;
  }

  /**
   * Validates ID of a token
   * @param  {Number} id    ID of token object.
   * @return {Number}       parsed integer.
   */
  #validateId(id) {
    if(id === undefined || isNaN(id))
      throw new ErrorBadRequest(
        "On id has to be a number."
      );

    return parseInt(id);
  }

  /**
   * [validateToken description]
   * @param  {Object} token Token object.
   * @return {Object}       Clone of token object.
   */
  #validateToken(token) {
    return this.#repository.validateToken(token);
  }
}
