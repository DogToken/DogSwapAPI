'use strict';

import Controller from './Controller.js';

import RepositoryUser from '../repositorys/RepositoryUser.js';
import ErrorBadRequest from '../errors/ErrorBadRequest.js';

import jwt from 'jsonwebtoken';
import readJsonSync from 'read-json-sync';
import sha256 from 'sha256';

/**
 * User controller to use in Routes.
 */
export default class ControllerAuth extends Controller {

  #repository;
  #pems;

  /**
   * Constructs the user controller.
   * @param  {Object} config Basic configurations
   */
  constructor(config) {
    super();
    this.#repository = new RepositoryUser(config.database);
    this.#pems = JSON.parse(readJsonSync('assets/pems.json'));
  }

  /**
   * Handels login Requsts
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  login = async (req, res) => {
    try {
      const auth = this.#verifyAuthBody(
        req.body
      );
      const password = sha256(auth.password).toUpperCase();

      const result = await this.#repository.findByEMail(auth.email);
      
      if(result && password == result.password){
        const token = {
          token: jwt.sign({ email: auth.email }, this.#pems.private, { algorithm: 'RS256'})
        };

        return res.status(200).json(token);
      } else {
        return res.status(422).json({message: "Wrong Credentials."});
      }
    } catch(e) {
      return this.handleResponsError(e, res);
    }
  };

  /**
   * [description]
   * @param  {Object}   req   Request object.
   * @param  {Object}   res   Response object.
   * @param  {Function} next  Passes control to the next matching route
   * @return {Object}         Returns the ServerResponse object.
   */
  auth = (req, res, next) => {
    const authorization = req.headers.authorization;
    
    if(authorization !== undefined && authorization.startsWith("Bearer ")){
      const token = authorization.substring(7, authorization.length);

      try {
        req.token = this.verifyJWT(token);
        next();
      } catch(e) {
        return res.status(401).json({message: e.message});
      }
    } else {
      return res.status(403).json({message: "No bearer token defined"});
    }
  };

  /**
   * Verify JWT token.
   * @param  {String} token JWT bearer token
   * @return {Object}       Decrypted token
   */
  verifyJWT(token){
      return jwt.verify(token, this.#pems.public);
  };

  /**
   * Checks the body of a login request.
   * @param  {Object} auth Body of request.
   * @return {Object}      Clone of body.
   */
  #verifyAuthBody(auth) {
    if(typeof auth != 'object')
      throw new ErrorBadRequest(
        "Body of auth request has to be an JSON object."
      );

    if(typeof auth.email != 'string')
      throw new ErrorBadRequest(
        "Property email of auth object has to be of type string."
      );

    if(typeof auth.password != 'string')
      throw new ErrorBadRequest(
        "Property password of auth object has to be of type string."
      );

    const pattern = /^.+@.+\..+$/;
    if(!pattern.test(auth.email))
      throw new ErrorBadRequest(
        "Property email of auth request has to be a valid e-mail address."
      );

    return { ...auth };
  }

}
