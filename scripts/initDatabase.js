import fs from 'fs';
import Database from 'mysql-promise-wrapper';
import readJsonSync from 'read-json-sync';

const sql = fs.readFileSync('assets/init.sql', 'UTF-8').toString();

const options_live = readJsonSync('configs/config.json');
const options_test = readJsonSync('configs/config.test.json');

const database_live = new Database({
  host     : options_live.database.host,
  user     : options_live.database.user,
  password : options_live.database.password,
  database : options_live.database.database,
  multipleStatements: true
});

const database_test = new Database({
  host     : options_test.database.host,
  user     : options_test.database.user,
  password : options_test.database.password,
  database : options_test.database.database,
  multipleStatements: true
});

(async () => {
  await database_live.connection((connection) => {
    return connection.query(sql);
  });

  await database_test.connection((connection) => {
    return connection.query(sql);
  });

  //Hack
  process.exit(1);
})();
