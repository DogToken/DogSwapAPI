'use strict';

import Controller from './Controller.js';

import Web3 from 'web3';
import BigNumber from 'bignumber.js';

import readJsonSync from 'read-json-sync';

/**
 * Token controller to use in Routes.
 */
export default class ControllerToken extends Controller {

  /**
   * Not used yet.
   */
  #repository;

  /**
   * ABI code of contract.
   */
  #swapContract;

  /**
   * Constructs the token controller.
   * @param  {Object} config Basic configurations
   */
  constructor(config) {
    super();

    const web3 = new Web3(
      new Web3.providers.HttpProvider(config.provider)
    );

    const DOGSWAPABI = readJsonSync("assets/abi/DOGSWAP.json");
    
    this.#swapContract = new web3.eth.Contract(
      DOGSWAPABI,
      config.DOGSWAPAddress
    );
  }

  /**
   * Returns all trades as a response.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  getAllTrades = async (req, res) => {
    try {
      const events = await this.#swapContract.getPastEvents(
        'newSWAP',
        {fromBlock: 0}
      );

      const trades = [];

      events.forEach((event) => {
        trades.unshift({
          blockNumber: event.blockNumber,
          sender: event.returnValues.sender,
          tokenIn: event.returnValues.tokenIn,
          tokenOut: event.returnValues.tokenOut,
          amountIn: this.#toDecimal(event.returnValues.amountIn, 12),
          amountOut: this.#toDecimal(event.returnValues.amountOut, 12)
        })
      });

      return res.status(200).json(trades);
    } catch(e) {
      this.handleResponsError(e);
      return;
    }
  };

  /**
   * Calculates a desimal from big int.
   * @param  {Number} value    Big nummber from blockchain.
   * @param  {Number} decimals Decimal places.
   * @return {Number}          Number as decimal.
   */
  #toDecimal(value, decimals) {
    const bigValue = new BigNumber(10);
    return bigValue.pow(-decimals).times(value).toNumber();
  }
}
